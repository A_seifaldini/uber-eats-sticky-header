import React, { useRef, useState } from "react";
import { StyleSheet, View } from "react-native";
import Animated from "react-native-reanimated";
import { onScrollEvent, useValues } from "react-native-redash";
import { SafeAreaProvider } from "react-native-safe-area-context";

import HeaderImage from "./HeaderImage";
import Content, { defaultTabs } from "./Content";
import Header from "./Header";
import { ScrollView } from "react-native-gesture-handler";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default () => {
  const scrollView = useRef<Animated.ScrollView>(null);
  const [tabs, setTabs] = useState(defaultTabs);
  const [y] = useValues([0], []);
  const onScroll = onScrollEvent({ y });
  return (
    <SafeAreaProvider>
      <View style={styles.container}>
        <HeaderImage {...{ y }} />
        <Animated.ScrollView
          ref={scrollView}
          scrollEventThrottle={1}
          {...{ onScroll }}
        >
          <ScrollView/>
          <Content
            onMeasurement={(index, tab) => {
              tabs[index] = tab;
              setTabs([...tabs]);
            }}
            {...{ y }}
          />
        </Animated.ScrollView>
        <Header {...{ y, tabs, scrollView }} />
      </View>
    </SafeAreaProvider>
  );
};
