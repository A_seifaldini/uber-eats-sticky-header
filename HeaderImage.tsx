import React from "react";
import { Dimensions, StyleSheet } from "react-native";
import Animated from "react-native-reanimated";
import { SliderBox } from "react-native-image-slider-box";

const { Extrapolate, interpolate } = Animated;
const { height: wHeight, width: wWidth } = Dimensions.get("window");
// eslint-disable-next-line @typescript-eslint/no-var-requires
export const backgroundImage = require("./assets/background.jpeg");

const data = [
  "https://images.unsplash.com/photo-1558980394-34764db076b4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1586608901658-70949a9a3c8a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
];

export const HEADER_IMAGE_HEIGHT = wHeight / 2;
const styles = StyleSheet.create({
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    width: wWidth,
    resizeMode: "cover",
  },
});

interface HeaderImageProps {
  y: Animated.Value<number>;
}

export default ({ y }: HeaderImageProps) => {
  const height = interpolate(y, {
    inputRange: [-100, 0],
    outputRange: [HEADER_IMAGE_HEIGHT + 100, HEADER_IMAGE_HEIGHT],
    extrapolateRight: Extrapolate.CLAMP,
  });
  const top = interpolate(y, {
    inputRange: [0, 100],
    outputRange: [0, -100],
    extrapolateLeft: Extrapolate.CLAMP,
  });
  return (
    // <Animated.Image
    //   source={backgroundImage}
    //   style={[styles.image, { top, height }]}
    // />
    <Animated.View style={[styles.image, { top, height }]}>
      <SliderBox
        images={data}
        sliderBoxHeight={HEADER_IMAGE_HEIGHT}
        circleLoop
      />
    </Animated.View>
  );
};
